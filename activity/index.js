


let registeredUsers = ["James Jeffries","Gunther Smith","Macie West","Mitchelle Queen","Shange Miguelito","Fernando Dela Cruz", "Akiko Yukihime"]


function checkUserExist(user){
	for(let i = 0; i<registeredUsers.length;i++){
		if(user.toLowerCase() === registeredUsers[i].toLowerCase()){
			return true
			break
		}			
	}
	return false
}


function register(user){

	if(!checkUserExist(user)){
		registeredUsers.push(user)
	}
	else{
		alert("User already exist!")
	}
	
}

let friendList = []

function addFriend(user){

	if(!checkUserExist(user)){
		alert("User not found")
		return
	}

	for(let i = 0; i<friendList.length;i++){
		if(user.toLowerCase() === friendList[i]){
			alert("Already a friend")

			return
		}
	}
	friendList.push(user)
}



function displayFriends(){
	if(!friendList.length>0){
		alert("No friends yet")
		return
	}

	friendList.forEach(function(user){
		console.log(user)
	})
}

function displayNumberOfFriends(){
	if(!friendList.length>0){
		alert("No friends yet")
		return
	}
	let i = 0

	friendList.forEach(function(user){
		i++
		
	})
	console.log("Number of Friends: " + i)
}

function deleteAFriend(){
	if(!friendList.length>0){
		alert("No friends yet")
		return
	}
	friendList.pop()
}

